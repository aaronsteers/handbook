---
layout: page
title: Leadership Team
weight: 2
---

The Meltano Leadership team is:

- Douwe Maan, CEO
- Emily Kyle, Director of Marketing
- AJ Steers, Head of Engineering
- Taylor Murphy, Head of Product & Data
- REB - Richard E. Baum, Head of Operations

The leadership team meets 2-4 times per month to have cross-functional discussions about how to best move the business forward.
Prior to each meeting, each member of the leadership team will share their top priorities for current and upcoming weeks in the Leadership Call agenda.
