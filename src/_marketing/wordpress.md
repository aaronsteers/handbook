---
layout: page
title: "WordPress Management"
weight: 2
---

We use WordPress to manage our marketing site and blog. This section contains information on how to manage various parts of the site.

## Update Slack link and Announcement Callouts

1. Navigate to the [Global Content](https://meltano.com/wp-admin/admin.php?page=theme-content) settings page.
1. Select "Side Navigation" from the "Theme Content" menu.
1. Modify text areas as needed.
1. (Optional) Update Slack icon or bell icon as needed. Please make sure to run this by marketing so that we can make sure the icons match the theme.

## Add Someone to the Team Section

The team section is currently hosted on the [About](https://meltano.com/about/) page.

**Double check that the teammate wants to appear on this page and is okay with their location being posted.**

1. Collect a headshot, location, and social media links from the teammate.
1. Navigate to the [WordPress Admin Console](https://meltano.com/wp-admin/) and select "Team" from the left-side menu. Select "Add Team" from the menu that pops up.
1. Fill out required information: First and Last Name, Position (job title), Location (City/State/Country)
1. Ignore the "Bio" section for now. It's not displayed anywhere.
1. If you have social links you can add them at the bottom. These are nice to have byt not required
1. Click "Publish" on the right-side menu.

## Remove Someone from Team Section

1. 1. Navigate to the [WordPress Admin Console](https://meltano.com/wp-admin/) and select "Team" from the left-side menu.
1. Hover over the profile you'd like to delete and click "Trash". This will move the profile to the [Trash section](https://meltano.com/wp-admin/edit.php?post_status=trash&post_type=team) where it can be restored if needed.

## Add New Partners

Before adding a partner, you'll need to collect the company name, a short blurb about the company, a URL to the company website, and a logo file (SVG or transparent PNG preferred) from the partner.

1. Navigate to the [Partners](https://meltano.com/wp-admin/edit.php?post_type=partners) page.
1. Click "Add Partner" at the top of the page.
1. Add the partner's name in the Title bar.
1. Copy and paste the partner blurb into the text area. Highlight the text and click "Align Center" at the top. This will center it on the partner page and make it look nice with our theme.
1. Click "Set Featured Image" on the right-side menu and upload the partner logo. Once uploaded you can then select it as the featured image.
1. Click publish once you're ready.
1. Navigate to the [Partner Re-Order page](https://meltano.com/wp-admin/edit.php?post_type=partners&page=order-post-types-partners) and make sure the new partner is sorted in the list alphabetically.
1. Click update. The partner should now be live on the site in the right order.

## Update Partners

1. Navigate to the [Partners](https://meltano.com/wp-admin/edit.php?post_type=partners) page.
1. Click the partner you'd like to edit and make changes accordingly.
1. Click "Publish" to make your changes public.

## Sort Partners

1. Navigate to the [Partner Re-Order page](https://meltano.com/wp-admin/edit.php?post_type=partners&page=order-post-types-partners) and make sure the partners are sorted in the list alphabetically.
1. Click update. The partner should now be live on the site in the right order.
